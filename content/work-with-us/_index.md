---
title: "Work With Us"
draft: false
menu: main
summary: "Join our research team for healthier, comfortable, and efficient indoor environments. Together, to make a positive impact."
weight: 60
bannerHeading: "Work With Us"
---

The Human Resources Division is responsible for recruiting prospective employees, employee and labor relations, payroll, compensation and classification, employee benefits, organizational development, and risk management.

Champaign County Regional Planning Commission is committed to being an employer of choice. Career opportunities cover a wide spectrum of fields including engineering, planning, law enforcement, social work, early child development, economic and community development, and much more.

Champaign County is an equal opportunity employer. Employment selection and related decisions are made without regard to sex, race, age, disability, religion, national origin, color, or any other protected class.

We are seeking qualified individuals who share a commitment to professionalism, respect and teamwork. We offer career opportunities that cover a wide spectrum of fields. Would you like to join our team?

To find the list of all available jobs, please visit [here](https://ccrpc.org/jobs/)
