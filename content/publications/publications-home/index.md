---
title: "Publications Home"
weight: 10
summary: " "
bannerHeading: "Publications"
---
## 2024

### <span style="color:darkorange">Reports</span>

Karion, A., Shepson, P., Merrin, Z., et al., (2024) _Recommendations from Workshop on Quantifying Methane Emissions Across Natural Gas Infrastructure in Urban Environments._ (National Institute of Standards and Technology, Gaithersburg, MD), NIST Special Publication (SP) NIST SP 1500-22. https://doi.org/10.6028/NIST.SP.1500-22

Sun, Y., Francisco, P.F., Merrin, Z., Gilleade, K., "CFD Simulations of Small Particle Behavior with Blower-Driven Airflows in Single-Family Residential Buildings", _Indoor Air_, vol. 2024, Article ID 6685891, 11 pages, 2024. https://doi.org/10.1155/2024/6685891 

### <span style="color:darkorange">Conference Extended Abstracts</span>

Wilson, J., Dixon, S. L., Francisco, P., Gilleade, K., Breysse, J., Sun, Y., Merrin, Z., Jacobs, D. E. (2024) _Comparing Bath vs Kitchen Continuous Exhaust Ventilation on Indoor Air Contaminants in Single-Family Detached Homes using Gas Cooking._ Indoor Air 2024: Proceedings of 18th International Conference on Indoor Air Quality and Climate, Honolulu, Hawaii.

## 2023

### <span style="color:darkorange">Reports</span>

Francisco, P. W., LaFleur, J., Sun, Y., Merrin, Z., Gilleade, K., Graham, M., Wilson, J., Jacobs, D. E., & Brand, L. (2023). _Energy Savings With Acceptable Indoor Air Quality Through Improved Airflow Control in Residential Retrofits_ (Report No. DOE/GO102023-5783). United States Department of Energy. https://doi.org/10.2172/1985923 

Abebe, F., Francisco, P. W., Merrin, Z., Curtis, S., Shupe, L. A., Price, N., Hunter, M., Johnson, V., Duden, G. & Other contributors. (2023). _Clean Jobs Curriculum Framework. Clean Jobs Illinois – Clean Jobs Workforce Network Program_. Illinois Department of Commerce & Economic Opportunity. https://dceo.illinois.gov/content/dam/soi/en/web/dceo/ceja/documents/clean-jobs-curriculum-framework.pdf 

## 2022

### <span style="color:darkorange">Journal Articles</span>

Christensen, P., Francisco, P. W., Myers, E., Shao, H., & Souza, M. (2022). Energy Efficiency Can Deliver for Climate Policy: Evidence from Machine Learning-Based Targeting. _Nat Bureau of Econ Res_. Working paper 30467. https://doi.org/10.3386/w30467 

Bongungu, J.L., Francisco, P. W., Gloss, S., & Stillwell, A. S. (2022). Estimating Residential Hot Water Consumption from Smart Electricity Meter Data. _Env Res: Infrastructure and Sustainability,_ 2(4), 045003. https://doi.org/10.1088/2634-4505/ac8ba2 

### <span style="color:darkorange">Report</span>

Merrin, Z., & Francisco, P. W. (2022). _Pilot Study for Multifamily Building Ventilation and Indoor Air Quality_ (Report No. ORNL/TM-2022/2672). Oak Ridge National Laboratory. https://doi.org/10.2172/1897828 

### <span style="color:darkorange">Book Chapter</span>

Bond, T.C., & Merrin, Z. (2022). Appliances for Cooking, Heating, and Other Energy Services. In: Zhang, Y., Hopke, P.K., Mandin, C. (Eds), _Handbook of Indoor Air Quality_. Springer, Singapore. https://doi.org/10.1007/978-981-10-5155-5_6-1

### <span style="color:darkorange">Workshops</span>

Francisco, P. W. (2022, May 4-6). _Ducts and Soil Contaminants: Evidence from Radon Studies_. ASHRAE IAQ 2020: Indoor Environmental Quality Performance Approaches, Athens, Greece. 

Gilleade, K., & Sun, Y. (2022, January 18-20). _Using low-cost sensors to manage indoor environmental quality in low-resource residential housing_. Healthy Buildings America 2021, Virtual. 

### <span style="color:darkorange">Conference Papers</span>

Sun, Y., Francisco. P. W., & Merrin, Z. (2022). _Modeling Contaminant Transport from Garage to Living Space in Residential Buildings Based on Single Tracer Gas Decay Measurements_. ASHRAE IAQ 2020: Indoor Environmental Quality Performance Approaches Athens, Greece. https://www.aivc.org/sites/default/files/2_C27.pdf 

### <span style="color:darkorange">Conference Extended Abstracts</span>

Merrin, Z., Francisco, P. W., & Gloss, S. (2022). _Agreement in Radon Variability Between Proximate Houses_. ASHRAE IAQ 2020: Indoor Environmental Quality Performance Approaches, Athens, Greece. https://www.aivc.org/sites/default/files/2_A6.pdf 

Gloss, S., Francisco, P. W., Dixon, S., Wilson, J., Merrin, Z., Breysse, J., Sun, Y., & Su, J. (2022) _Impact of Precautionary Measures on Indoor Radon Levels in Retrofit Homes_. ASHRAE IAQ 2020: Indoor Environmental Quality Performance Approaches Athens, Greece. https://www.aivc.org/sites/default/files/2_A8.pdf 

Sun, Y., Francisco, P.W., & Gilleade, K. (2022, January 18-20). _CFD Simulations of exhaled droplet pathways in a house room with a blower door fan to reduce fan to reduce virus airborne transmissions_. Healthy Buildings America 2021, Virtual. 

Gilleade, K., Francisco, P. W., & Merrin, Z. (2022, January 18-20). _Building Negative Pressure Rooms in Residential Households to Support COVID-19 Patients_. Healthy Buildings America 2021, Virtual. 

Merrin, Z., & Francisco, P. W. (2022, January 18-20). _IAQ and Connectivity Before and After Multifamily Weatherization – and Field Research Pandemic Procedures_. Healthy Buildings America 2021, Virtual.

Sun, Y., Rose, W. B., Lafleur, J., Merrin, Z., & Francisco, P. W. (2022, June 12-16). _Systematic Airflow Management for Energy Savings and Indoor Air Quality in Residential_. Indoor Air 2022: Proceedings of 17th International Conference on Indoor Air Quality and Climate, Kuopio, Finland.

Sun, Y., Gilleade, K., Francisco, P. W., Merrin, Z., Rose, W. B., & Lafleur, J. (2022, June 12-16). _Comparisons of Exhaust and Supply Ventilation Strategies on Indoor Air Quality_. Indoor Air 2022: 17th International Conference on Indoor Air Quality and Climate, Kuopio, Finland. 

## 2021

### <span style="color:darkorange">Journal Article</span>

Christensen, P., Francisco, P. W., Myers, E. & Souza, M. (2023, online: 2021). Decomposing the Wedge between Projected and Realized Returns in Energy Efficiency Programs. _The Review of Economic and Statistics_, 105(4): 798-817. https://doi.org/10.1162/rest_a_01087 

## 2020

### <span style="color:darkorange">Report</span>

Francisco, P. W., Gloss, S., Wilson, J. Sun, Y. Dixon, S.L., Merrin, Z., Breysse, J., Tohn, E., & Jacobs, D. E. (2020). _Building Assessment of Radon Reduction Interventions with Energy Retrofits Expansion (The BEX Study): Final Report_ (Report No. ORNL/TM-2020/1769). Oak Ridge National Laboratory. https://weatherization.ornl.gov/wp-content/uploads/2021/01/ORNL-_TM-2020_1769.pdf 

## 2019

### <span style="color:darkorange">Report</span>

Sowers, R. B., & Francisco, P. W. (2019). _DPI Workshop Summary Report_.

### <span style="color:darkorange">Journal Articles</span>

Francisco, P.W., Gloss, S., Wilson, J., Rose, W. B., Sun, Y., Dixon, S.L., Breysse, J., Tohn, E., & Jacobs, D. E. (2020, online: 2019). Radon and Moisture Impacts from Interventions Integrated with Housing Energy Retrofits. _Indoor Air_ 30(1): 147-155, https://doi.org/10.1111/ina.12616. 

Merrin, Z., & Francisco, P.W. (2019). Unburned Methane Emissions from Residential Natural Gas Appliances. Environmental Science & Technology 53(9), 5473-5482. https://doi.org/10.1021/acs.est.8b05323 

## 2018

### <span style="color:darkorange">Journal Articles</span>

Merrin, Z., Francisco, P.W., Bohac, D., Quinnell, J. A., & Olson, C. (2018). An evaluation of strategies to reduce transport of pollutants from garages to homes. _Science and Technology for the Built Environment_, 24(2), 198-208. https://doi.org/10.1080/23744731.2017.1417664 

Pigg, S., Cautley, D., & Francisco, P. W. (2018). Impacts of weatherization on indoor air quality: A field study of 514 homes. _Indoor Air_, 2018 Mar, 28(2), 307-317. https://doi.org/10.1111/ina.12438 

### <span style="color:darkorange">Conference Extended Abstracts</span>

Sun, Y., Francisco, P. W., Bond, T., Verma, V., Nguyen, T. H. (2018) _Temporal and spatial variability of multiple contaminants in a residence_. INDOOR AIR 2018: 15th Conference of the International Society of Indoor Air Quality and Climate, Philadelphia, PA.

## 2017

### <span style="color:darkorange">Journal Articles</span>

Francisco, P. W., Pigg, S., Cautley, D., Rose, W. B., Jacobs, D. E., & Cali, S. (2017). Carbon monoxide measurements in homes. _Science and Technology for the Built Environment,_ 24(2), 118-123. https://doi.org/10.1080/23744731.2017.1372806 

Francisco, P.W., Jacobs, D. E., Tragos, L. Dixon, S.L., Breysse, J., Rose, W. B., & Cali, S. (2017, online: 2016). Ventilation, Indoor Air Quality, and Health in Homes Undergoing Weatherization. Indoor Air 27(2), 463-477. http://doi.org/10.1111/ina.12325 

### <span style="color:darkorange">Report</span>

Merrin, Z., Bohac, D., Quinnell, J. A., & Olson, C. (2017). Transport of Contaminants from Garages _Attached or Integral to Low-Rise Residential Buildings_ (Report No. D-RP-1450). ASHRAE. https://www.techstreet.com/standards/rp-1450-transport-of-contaminants-from-garages-attached-or-integral-to-low-rise-residential-buildings?product_id=1990679 

### <span style="color:darkorange">Magazine Articles</span>

Rose, W. B. (2017, February 3). Bill Rose’s Building Science To-Do List: Rose invites young building scientists to investigate six topics. _Green Building Advisor_. http://www.greenbuildingadvisor.com/articles/dept/musings/bill-rose-s-building-science-do-list

Francisco, P.W., Jacobs, D. E., Rose, W. B., & Cali, S. (2017, May). Mechanical Ventilation’s Impact on IAQ. _ASHRAE Journal_ 59(5), 96-97. https://search.proquest.com/openview/6eb323d349d9acb18949c847149058e4/1?pq-origsite=gscholar&cbl=41118 

## 2016

### <span style="color:darkorange">Conference Papers</span>

Walker, I., Sherman, M., & Francisco, P. W. (2016). _Duct Leakage Repeatability Testing_. Proceedings of the Performance of Exterior Envelopes of Whole Buildings XIII Conference, Clearwater, FL. https://www.techstreet.com/ashrae/standards/duct-leakage-repeatability-testing?product_id=1941293 

Lubliner, M., Francisco, P. W., Martin, E., Walker, I., Less, B., Vieira, R., Kunkle, R. & Merrin, Z. (2016). _Practical Applications and Case Study of Temperature Smart Ventilation Controls_. Proceedings of the Performance of Exterior Envelopes of Whole Buildings XIII Conference, Clearwater, FL. https://www.techstreet.com/standards/practical-applications-and-case-study-of-temperature-based-smart-ventilation-controls?product_id=1940087

Rose, W. B., & Gloss, S. (2016). Characterizing Indoor Humidity for Comparison Studies: _the Moisture Balance Approach_. Proceedings of the Performance of Exterior Envelopes of Whole Buildings XIII Conference, Clearwater, FL. https://www.techstreet.com/ashrae/standards/characterizing-indoor-humidity-for-comparison-studies-the-moisture-balance-approach?product_id=1940161 (presentation) 

Francisco, P.W., Brand, L., Cautley, D., Singer, B., & Gloss, S. (2016). _The latest developments in residential combustion safety testing_. Proceedings of the ASHRAE IAQ 2016: Defining Indoor Air Quality: Policy, Standards and Best Practices, Alexandria, VA. https://www.techstreet.com/ashrae/standards/the-latest-developments-in-residential-combustion-safety-testing?product_id=1928365 

Francisco, P. W., Pigg, S., Cautley, D., Rose, W. B., Jacobs, D., & Cali, S. (2016). _Carbon Monoxide Measurements in Homes_. Proceedings of the ASHRAE IAQ 2016: Defining Indoor Air Quality: Policy, Standards and Best Practices, Alexandria, VA. https://www.techstreet.com/ashrae/standards/carbon-monoxide-measurements-in-homes?product_id=1928358 

Merrin, Z., Francisco, P. W., Bohac, D., Quinnell, J. A., & Olson, C. (2016). Attached Garages: _IAQ Implications and Solutions_. Proceedings of the ASHRAE IAQ 2016: Defining Indoor Air Quality: Policy, Standards and Best Practices, Alexandria, VA. https://www.techstreet.com/ashrae/standards/attached-garages-iaq-implications-and-solutions?product_id=1928346 

Lubliner, M., Francisco, P. W., Martin, E., Walker, I., Less, B., Vieira, R., Kunkle, R. & Merrin, Z. (2016). _Practical Applications and Case Study of Temperature Smart Ventilation Controls_. Proceedings of the ASHRAE IAQ 2016: Defining Indoor Air Quality: Policy, Standards and Best Practices, Alexandria, VA. https://www.techstreet.com/ashrae/standards/practical-applications-and-case-study-of-temperature-based-smart-ventilation-controls?product_id=1928349

### <span style="color:darkorange">Conference Extended Abstracts</span>

Merrin, Z., Francisco, P. W., Bond, T. C., Lam, N. L., Thompson, R., & Weyant, C. (2016). _Air Quality Implications of Heating with Wood Stoves_. Indoor Air 2016: Proceedings of 14th International Conference on Indoor Air Quality and Climate, Ghent, Belgium. https://www.isiaq.org/docs/Papers/Paper989.pdf

Merrin, Z., Francisco, P. W., & Gloss, S. _Residential Natural Gas Emissions from Pipe Leaks and Heating Appliances_. Indoor Air 2016: Proceedings of 14th International Conference on Indoor Air Quality and Climate, Ghent, Belgium. https://www.isiaq.org/docs/Papers/Paper987.pdf

### <span style="color:darkorange">Magazine Article</span>

Francisco, P. W., & Sherman, M. H. (2016). Addressing Residential. _ASHRAE Journal_, 58(5), 84-86. https://search.proquest.com/openview/e33d4008c095c42ba7820f882dadc165/1?pq-origsite=gscholar&cbl=41118 

## 2015

### <span style="color:darkorange">Reports</span>

Brand, L., Cautley, D., Bohac, D., Francisco P. W., Shen, L, & Gloss, S. (2015). _Technology Solutions Case Study: Combustion Safety Simplified Test Protocol Field Study_ (Report No. DOE/GO-102015-4806). United States Department of Energy Partnership for Advanced Residential Retrofit and NorthernSTAR (Subcontract KNDJ-0-40346-05). https://www.osti.gov/biblio/1233566 

Francisco, P. W., Merrin, Z., & Gloss, S. (2015). _Development of Test Methodology and Initial Pilot Field Testing for Residential Methane Emissions Quantification_. Environmental Defense Fund. 

Francisco, P. W., Jacobs, D. E., Targos, L., Dixon, S. L., Breysse, J., Rose, W. B., Dawson, H., Nall, S., Phillips, D., & Cali, S. (2016). _Ventilation, Indoor Air Quality and Health Outcomes in Home Weatherization_. United States Department of Housing and Urban Development (Grant Number ILLHH0230-10).

Rose, W. B., Francisco, P. W. & Merrin, Z. (2015). _Low Cost Radon Reduction Pilot Study_. U.S. Department of Energy/National Renewable Energy Laboratory Building America, Partnership for Advanced Residential Retrofit (subcontract KNDJ-0-40346-04). https://www1.eere.energy.gov/buildings/publications/pdfs/building_america/low-cost-radon-reduction-pilot-study.pdf

### <span style="color:darkorange">Conference Papers</span>

Rose, W. B., Gloss, S., Francisco, P. W., Merrin, Z. Cali, S. & Jacobs, D. E. (2015) _Weatherization Ventilation Strategy as a Factor in Indoor Humidity_. Proceedings of Healthy Buildings America, Boulder, CO. https://www.isiaq.org/docs/Proceedings_HB2015-America.pdf#page=279 

Rose, W. B., Merrin, Z. Francisco, P. W., & Gloss, S. (2015). _Low Cost Radon Reduction Pilot Study: Air-tightening at Foundation-house Interface as a Means of Radon Reduction in Living Space_. Proceedings of Healthy Buildings America, Boulder CO. https://www.isiaq.org/docs/Proceedings_HB2015-America.pdf#page=536 

## 2014

### <span style="color:darkorange">Report</span>

Pigg, S., Cautley, D., Francisco, P. W., Hawkins, B, & Brennan, T. (2014) _Weatherization and Indoor Air Quality: Measured Impacts in Single-family Homes under the Weatherization Assistance Program_ (Report No. ORNL/TM-2014/170). United States Department of Energy (Contract No. DE-AC05-00OR22725). https://weatherization.ornl.gov/wp-content/uploads/pdf/WAPRetroEvalFinalReports/ORNL_TM-2014_170.pdf

### <span style="color:darkorange">Conference Papers</span>

Francisco, P. W. (2014, July). _ASHRAE Standard 62.2: What’s New and Why_. Proceedings of Indoor Air: 13th International Conference on Indoor Air Quality and Climate, Hong Kong. https://www.isiaq.org/docs/paper/HP1098.pdf 

Rose, W.B., Francisco, P. W. & Merrin, Z. (2014, July). _Radon Reduction through Floor Air Sealing_, Proceedings of Indoor Air 2014: 13th International Conference on Indoor Air Quality and Climate, Hong Kong. https://www.isiaq.org/docs/paper/HP1019.pdf 


## 2013

### <span style="color:darkorange">Conference Papers</span>

Francisco, P. W., Cali, S. Jacobs, D. E., Rose, W. B., Merrin, Z., & Targos, L. (2013). _Measured Impact of Ventilation Rate Determination Strategy on IAQ Parameters_. Conference proceeding ASHRAE IAQ 2013. https://www.techstreet.com/ashrae/standards/measured-impact-of-ventilation-rate-determination-strategy-on-iaq-parameters?product_id=1869509 

### <span style="color:darkorange">Magazine Article</span>

Francisco, P. W. (2013). _Unvented Combustion_. ASHRAE Journal 55(4), 64067