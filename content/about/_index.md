---
title: "About"
draft: false
menu: main
summary: "Indoor Climate Research and Training (ICRT) is focused on using sound principles of building science to advance the performance of homes by developing and disseminating knowledge and best practice through education, training, and research."
weight: 10
bannerHeading: "About"
bannerText: >
  ICRT’s mission is “to bridge the gaps between energy & health, and between research & practice to improve real outcomes for residents”.
---

## Who We Are

Indoor Climate Research and Training (ICRT) is focused on using sound principles of building science to advance the performance of homes by developing and disseminating knowledge and best practice through education, training, and research. ICRT conducts basic research at the intersection of energy efficiency and indoor air quality (IAQ). ICRT maintains a state-of-the-art training center and administers the training program for the Illinois Home Weatherization Assistance Program (IHWAP), which provides weatherization services to low-income Illinois residents and households. Research projects include studies on radon, ventilation, combustion safety, and the health consequences related to IAQ. ICRT partners with other academic institutions, research and advocacy groups, and government agencies to execute research and translate those results and feedback from the field into actionable practices and policies.

ICRT staff are involved in setting national policy and standards for residential building energy efficiency and indoor air quality, through membership on committees and advisory boards.

## Origins

ICRT tracks its origins to the end of World War II when the University established the Small Homes Council to perform applied research on economically producing efficient and high quality homes for returning veterans. The Small Homes Council transitioned into the Building Research Council in the early 1990s, when they expanded their scope to include commercial building research.

In 2010, while still part of the Building Research Council, ICRT staff received a grant from the American Recovery and Reinvestment Act to establish the state’s first weatherization training center, to serve the needs of the Illinois Home Weatherization Assistance Program (IHWAP). This team spun out of the Building Research Council and took on the name Indoor Climate Research & Training in 2012. In 2023, ICRT became a division under the Champaign County Regional Planning Commission. ICRT was designed to provide job training and serve Illinois’ low-income households by facilitating home improvements intended to lower energy costs and improve home quality, and to conduct energy and IAQ research to improve outcomes for residents.

## Accredited Training

ICRT is accredited by the Interstate Renewable Energy Council (IREC) for its Energy Auditor and Quality Control Inspector training programs. ICRT is also certified as a Building Performance Institute [(BPI)](https://www.bpi.org/) test center. The program trains and certifies employees from the [30 local community action agencies](https://www.illinois.gov/) throughout Illinois. ICRT graduates perform weatherization work such as energy audits, quality control inspections, insulation, air sealing, and HVAC work, which includes cleaning, repairing, and upgrading equipment. The [Illinois Weatherization Training and Certification Program](https://ccrpc.gitlab.io/icrt/training/) (TCP) currently provides eleven courses for a total of over two hundred hours of required instruction to new local agency staff. ICRT also administers supplemental trainings, such as a one-week curriculum for training and certifying third-party IHWAP contractors, and a one-week curriculum for training energy auditors and quality control inspectors to be Healthy Home Evaluators.

## Transformative Research

In addition to its training efforts, ICRT performs [sponsored research](https://ccrpc.gitlab.io/icrt/research/) on residential building science. Current research efforts focus on the interaction of energy efficiency and indoor air quality with the goal of providing indoor environments that are safe, healthy, comfortable, and efficient. Sponsors include federal and state governments, non-profits, and industry organizations. ICRT conducts research in partnership with other researchers within and outside of universities. Research projects are often conducted in collaboration with practitioners and industry leaders which facilitates bringing research results to scale.

<!--
## About the Applied Research Institute

ICRT is part of the Applied Research Institute [(ARI)](https://appliedresearch.illinois.edu/) within University of Illinois College of Engineering. ARI is a unique university-based laboratory, where industry and federal clients connect with researchers in an integrated environment to understand their needs and rapidly bring solutions to the market. ARI provides rapid, cost-effective solutions to problems of mutual interest across sectors and agencies of government, with a sharp focus on the commercial development of technologies that are validated before they leave the laboratory. -->

## ICRT Mission

ICRT’s mission is “to bridge the gaps between energy & health, and between research & practice to improve real outcomes for residents”.

ICRT aims to:

* Perform applied research intended to improve housing conditions throughout the United States

* Provide strategies for maintaining healthy environments without sacrificing energy-efficiency

* Positively influence policy and standards through participation in committees, societies, and boards

* Integrate research and practice through joint projects and educational outreach

* Increase the number of active research grants, collaborating with other energy efficiency constituents

* Continuously apply innovation to research and educational opportunities, as well as departmental operations
