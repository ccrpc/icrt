---
title: "Natural Gas Appliances"
draft: false
weight: 50
summary: " "
bannerHeading: "Natural Gas Appliances"
---

##

Natural gas is a useful fuel, with high heat content, and clean emissions (relative to other fossil fuels). It is commonly used for residential space heating, water heating, and cooking. Natural gas is predominantly composed of methane, which is a potent greenhouse gas, so emissions of unburned methane should be minimized. Natural gas emissions can happen through leaks in piping infrastructure, or though the appliance exhaust as gas that does not get combusted by the appliance. High concentrations of methane in the exhaust gas indicate an appliance that is not functioning properly and should be repaired or replaced. Gas leaks from piping infrastructure can allow gas concentrations to reach dangerous levels indoors and potentially result in fires or explosions. If you smell natural gas in your house, leave the premise immediately, and contact your local gas utility or fire department.
