---
title: "Ventilation in Multifamily Buildings"
draft: false
weight: 70
summary: " "
bannerHeading: "Ventilation in Multifamily Buildings"
---

##

Ventilation in multifamily buildings is a complicated and controversial issue. Of course it is important for occupants in multifamily buildings to have fresh and clean air. However, with typical ventilation methods it is difficult to ensure the quantity of that fresh air (while competing with natural pressures induced by tall buildings), and it is difficult to guarantee where that air is coming from (could be from neighboring units). Current research is exploring how best to address the issues associated with ventilation in multifamily buildings.
