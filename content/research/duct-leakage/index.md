---
title: "Duct Leakage"
draft: false
weight: 40
summary: " "
bannerHeading: "Duct Leakage"
---

##

Duct leakage from forced-air HVAC systems has both Indoor Air Quality (IAQ) and energy efficiency implications. Duct leaks can waste energy by pushing conditioned air outside of the building, or by sucking unconditioned air into the system. Duct leaks can impact IAQ by drawing undesirable air in through the leaks in the return, and distributing it through the house through the air distribution system. Imbalances in duct systems can induce pressure differences in parts of a building, leading to unintended consequences such as backdrafting of a gas water heater.

There are a variety of tests that can be done to check for the presence and severity of duct leakage; testing can be for total duct leakage or just the portion of duct leakage to outdoors. Duct leaks can be addressed with approved foil tapes (NOT duct tape, despite the name), duct mastic, or rigid materials.
