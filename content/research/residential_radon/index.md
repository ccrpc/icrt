---
title: "Residential Radon"
draft: false
weight: 10
summary: " "
bannerHeading: "Residential Radon"
---

##

ICRT conducts research on the impact that typical energy saving retrofits have on radon levels and ways to prevent elevating radon when conducting energy retrofits. ICRT also conducts research to understand the drivers of varying radon concentrations.

## About Radon

Radon is a colorless odorless gas that is naturally produced by the decay process of elements commonly present in some rocks and soils. Radon is prevalent throughout most of the United States, including high concentrations in Illinois.

Radon exposure is [hazardous to human health](https://www.epa.gov/radon/health-risk-radon); according to the U.S. Environmental Protection Agency it is the second leading cause of lung cancer in the U.S. after smoking. Radon can enter a home through gaps and bypasses in the foundation, and building characteristics can cause accumulation leading to unhealthy concentrations. Typical radon remediation involves creating a barrier to impede the ingress of radon, and installing a fan to exhaust it outside. For more information about radon, please refer to this guide from the Environmental Protection Agency.

Because radon concentrations vary with time, the best way to determine radon levels in a home is to conduct a long-term (90+ day) test; a year is better. Short-term tests can also be used, and are often best considered to be screening tests. To find a licensed radon measurement or mitigation professional in Illinois, please refer the [Illinois Emergency Management Agency](https://www.iemaohs.illinois.gov/nrs/radon/radonillinois-nonflash.html)

## Our Research Studies

- Evaluating the effects of common weatherization retrofit activities on radon levels in homes in 4 states (2017-2019)
- Evaluating the effectiveness of low-cost measures at preventing increases of radon in the course of energy retrofits in residential buildings in two states (2014-2016)
- Understanding correlations of radon to weather / moisture patterns in the environment in Illinois (years)
- Assessing the effectiveness of air sealing between the first floor and foundation spaces for limiting radon concentrations (years)
- Determining the impact of installing ventilation, in the context of energy retrofits, on contaminant levels including radon
- Estimating the impact of 2010-era energy retrofits on radon
