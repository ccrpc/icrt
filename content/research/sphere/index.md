---
title: "SPHERE"
draft: false
weight: 90
summary: " "
bannerHeading: "SPHERE"
---

## About SPHERE

People spend 90% of their lives indoors and 70% at home. Housing and health are both human rights that are fundamental to the quality of life.

ICRT partners with [SPHERE](https://hiwater.org/sphere/), a coalition of scientists, engineers, practitioners, and community groups organized for collaborative, integrative work on the chemical, physical, and biological health stressors in the home environment.

SPHERE is an initiative of faculty at Colorado State University, the University of Illinois, and Washington University – St. Louis.

[Stuck in a Healthier Home: Ventilation Tips for the Housebound](https://hiwater.org/sphere/stuck_healthier/)
