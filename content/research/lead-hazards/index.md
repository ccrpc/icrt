---
title: "Lead Hazards"
draft: false
weight: 80
summary: " "
bannerHeading: "Lead Hazards"
---

##

Lead is a dense metallic chemical element with a wide variety of useful applications. It is also highly toxic to humans, especially children, negatively affecting the brain, and kidneys (as well as other organs and systems), and potentially leading to death. Lead was a popular additive for paints because it increases moisture resistance and durability, and was especially common in exterior and window paints. Lead household paint was banned in the US in 1978, but it is still regularly found in building built before that time. Exposure to lead in paint happens when the paint deteriorates and paint chips or paint dust are directly or indirectly ingested. When doing retrofit work (especially dust generating activities like sanding or demolition), it is important to know if there is lead based paint present, and to use the appropriate precautions. For more information on lead exposure and how to protect yourself, see [this guide from the US EPA.](https://www.epa.gov/lead/protect-your-family-sources-lead)

Performs research on lead hazard control for the Illinois Department of Public Health
An evaluation of lead education and window replacement In Illinois (2012-2015)
