---
title: "Garage Contaminant Transport"
draft: false
weight: 60
summary: " "
bannerHeading: "Garage Contaminant Transport"
---

## About Garage Contaminants as Indoor Air Pollutants

Garages can be major sources of IAQ pollutants including exhaust from cars, yard equipment and generators, fumes from paints, and chemicals (including those within your car itself). When garages are attached to houses, those pollutants can migrate through the garage-house interface and into the living space, increasing exposure. Garages with more common surfaces (e.g tuck under) will typically communicate more with the living space than garages with less (e.g. attached on one side). Houses with furnaces located in the garage or forced-air-conditioned garages can have additional communication when pressures induced by the air handler can pull or push garage air into the house through the duct system.

Steps to minimize contaminant transport into the living space include air-sealing of the house-garage interface and depressurizing the garage with mechanical ventilation. One common problem that is easy to fix are crawl space access hatches that are accessed from the garage; these can be made much more airtight through the use of solid materials like plywood instead of loose or connected separate planks. Other gaps, cracks, or baypasses in the garage-houses interface (especially the corners, near the sill and top plates, and cutouts around doors) can be addressed with rigid materials, caulks, tapes, and foams. Homes with furnaces in the garages should air seal the furnace cabinet and all ductwork in the garage. In our findings, air sealing the garage ducts was never effective enough to eliminate that path of exposure.

In addition to air sealing, depressurizing the garage with an exhaust fan is the best way to ensure that contaminant transport is minimized. There is no one-size-fits-all solution to garages, and the fan size best fan size for a garage will depend on the leakiness to both inside and outside, the size, design, and condition of the garage-house interface, and the type and magnitude of pollutants generated in the garage. For example, a tuck under garage with an older car that backs in, dust-generating activities (like metal work or wood cutting), and lots of paint and pesticide storage will need a larger fan than a side-attached garage with an electric car and not much else. In general, ICRT believes that a fan with ~100 CFM (Cubic Feet per Minute) of flow per number of car bays will be sufficient for most garages without excessive bypasses in the garage-house interface.

## Our Research

Evaluating strategies to reduce transport of pollutants from attached garages into homes. Read more about our research in [Science and Technology for the Built Environment.](https://www.tandfonline.com/doi/abs/10.1080/23744731.2017.1417664?journalCode=uhvc21)

## News Story

<!-- ![test](garage.jpg) -->
<!--
{{<image src="garage.jpg" alt="Description of the image" position="full" width="450px" height="150px" class="image-link">}} -->
<img src="garage.jpg" alt="Description of the image" width="250" height="150">

[ICRT FINDS SIMPLE SOLUTIONS TO INDOOR AIR QUALITY](https://appliedresearch.illinois.edu/newsevents)
