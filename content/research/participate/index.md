---
title: "Participate"
draft: false
weight: 100
summary: " "
bannerHeading: "Participate"
---

##

[Multifamily IAQ Study in New York](https://ccrpc.gitlab.io/icrt/quick-links/new-york/)

[MultiFamily IAQ Study in Chicago](https://ccrpc.gitlab.io/icrt/quick-links/multifamily-iaq-study-in-chicago/)

[New Home Indoor Air Quality Study](https://ccrpc.gitlab.io/icrt/quick-links/new-indoor-air-quality-study/)

Indoor Climate Research and Training frequently recruits home owners to enroll their residences into our field research program.

Click on the links in the Participate menu to learn more about our current projects and how to join.
