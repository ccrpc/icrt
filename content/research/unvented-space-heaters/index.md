---
title: "Unvented Space Heaters"
draft: false
weight: 30
summary: " "
bannerHeading: "Unvented Space Heaters"
---

## About Unvented Space Heaters

Unvented space heaters release all of their combustion emissions into the living space. Although this makes them very efficient (no waste heat escaping through the chimney), they can also lead to indoor air quality (IAQ) concerns. Although natural gas burns fairly cleanly, emissions will still include a small amount of carbon monoxide (CO), other pollutants like nitrogen oxide species (NOx), and of course a lot of water vapor.

## Our Research

Measured concentrations of combustion gases from the use of unvented gas fireplaces (2010). [Learn more about this research.](https://onlinelibrary.wiley.com/doi/10.1111/j.1600-0668.2010.00659.x)
