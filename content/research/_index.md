---
title: Research
weight: 20
menu: main
summary: "ICRT is currently involved in several efforts to promote improved residential building performance, focusing on the interaction of energy efficiency and indoor air quality to provide healthy, comfortable, and efficient indoor environments."
bannerHeading: "Research"
---

ICRT is currently involved in several efforts to promote improved residential building performance. Our research focuses on the interaction of energy efficiency and indoor air quality with a goal of providing indoor environments that are healthy, comfortable, and efficient.

Click on the links below or under the top navigation section to gain more information about our current projects.