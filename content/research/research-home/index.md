---
title: "Research Home"
draft: false
weight: 10
summary: " "
bannerHeading: "Research"
bannerText: >
  ICRT emphasizes field based data collection in its research by observing conditions before, during, and after interventions to the site.
---

ICRT is currently involved in several efforts to promote improved residential building performance. Our research focuses on the interaction of energy efficiency and indoor air quality with a goal of providing indoor environments that are healthy, comfortable, and efficient.

ICRT emphasizes field based data collection in its research. Projects often investigate the impacts of modifications to buildings by observing conditions before, during, and after interventions to the site.

![test10001](research__1.jpg)

### Participate in Research

Indoor Climate Research and Training frequently recruits home owners to enroll their residences into our field research program.

[Multifamily IAQ Study in New York](https://ccrpc.gitlab.io/icrt/quick-links/new-york/)

[MultiFamily IAQ Study in Chicago](https://ccrpc.gitlab.io/icrt/quick-links/multifamily-iaq-study-in-chicago/)

[New Home Indoor Air Quality Study](https://ccrpc.gitlab.io/icrt/quick-links/new-indoor-air-quality-study/)

Indoor Climate Research and Training frequently recruits home owners to enroll their residences into our field research program.

Click on the links in the Participate menu to learn more about our current projects and how to join.
