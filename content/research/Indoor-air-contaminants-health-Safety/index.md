---
title: "Indoor Air Contaminants, Health & Safety"
draft: false
weight: 20
summary: " "
bannerHeading: "Indoor Air Contaminants, Health & Safety"
---

##

Indoor Air Quality (IAQ) influences human comfort and health as well as building condition and resiliency. ICRT has conducted field studies evaluating multiple contaminants, including carbon monoxide (CO), carbon dioxide (CO2), respirable particulate matter (PM2.5), nitrogen dioxide (NO2), excessive moisture, radon, formaldehyde, volatile organic compounds, and methane.

## About Indoor Air Contaminants

[Carbon Monoxide (CO)](https://www.epa.gov/indoor-air-quality-iaq/carbon-monoxides-impact-indoor-air-quality) – a colorless odorless gas which is generated from incomplete combustion. CO can can be fatal at moderate concentrations, and cause headache, nausea, vomiting, dizziness, and fatigue at lower concentrations. Ensuring that all combustion appliances are properly vented to outside, and well maintained is the best way to minimize the risks of CO exposure.

Carbon Dioxide (CO2) – a colorless odorless gas which humans generate naturally as part of the respiration process. CO2 is not a health hazard at ambient concentrations, but at elevated concentrations it can cause cognitive impairment and drowsiness, and at very high levels can lead to dizziness, headache, and asphyxiation. Sufficient ventilation is the best way to maintain healthy CO2 levels.

[Particulate matter (PM)](https://www.epa.gov/indoor-air-quality-iaq/indoor-particulate-matter) – is a mixture of particles suspended in the air, varying in size and other attributes. PM comes from a variety of sources, including outdoor air, unvented combustion (gas appliances, smoking, incense, candles), cooking, and some indoor activities. PM is often categorized by its size as PM2.5 (particles smaller than 2.5 micrometers), and PM10 (particles smaller than 10 micrometers). Smaller particles are easily inhaled, and can penetrate deeply into lung tissue, impairing breathing function. Exposure to PM has been linked to irritation of the eyes, nose, and throat, as well heart and lung disease and premature mortality. PM exposure can be reduced by minimizing indoor PM generating activities, and supplying sufficient ventilation (possibly with filtration in areas with poor outdoor air quality).

[Moisture](https://www.epa.gov/iaq-schools/moisture-control-part-indoor-air-quality-design-tools-schools) – Water is essential for sustaining life, however too much or too little water in the air can cause discomfort for people, or damage to building materials. Moisture levels in air are measured by relative humidity (RH) or vapor pressure. RH is the most commonly used metric and refers to the amount of moisture that air can contain at a given temperature. RH that is too low it can cause issues with dry skin or irritated nasal passages, RH that is too high it can make the air feel stuffy, cause overheating, and lead to biological growth (mold, pests, etc.) on and in building materials. Excessive indoor moisture can be fixed by eliminating sources of liquid water (leaking pipes, accumulation in basements/crawl spaces), and liming unvented combustion.

[Volatile Organic Compounds (VOCs)](https://www.epa.gov/indoor-air-quality-iaq/volatile-organic-compounds-impact-indoor-air-quality) – are a complicated mixture of airborne chemicals. Some VOCs are benign while others can be highly toxic; some may smell pleasant while others can be noxious or irritating. VOCs can come from cleaning and painting supplies, air “freshening” aerosols, evaporation from liquids, and off-gassing from building materials and furniture. Indoor VOC levels can be minimized by ventilation, and through source control: by limiting the chemicals used indoors and storing them properly, and by selecting unscented and low-VOC products.

## Our Research Studies

Measuring emissions from new and improved solid fuel cookstoves in India (2013);
Combustion safety field testing; air quality impacts of wood heating stoves used in rural Alaskan villages (2015 – 2018).
Residential retrofit focused on radon, ventilation, combustion safety, and air leakage (2011-2015)
Evaluating the energy savings and indoor air quality impacts of retrofits performed under DOE’s low-income Weatherization Assistance Program
