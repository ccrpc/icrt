---
title: "Resources"
weight: 90
menu: main
draft: false
summary: "The mission of ICRT involves sharing valuable industry-based knowledge through credible sources to inform best practices in training and research."
bannerHeading: "Resources"
---

### External Links

Part of ICRT’s mission is to disseminate valuable industry-based knowledge through information sharing. ICRT turns to credible sources such those listed below to help inform its use of best practices in both training and research.

1. [American Society of Heating, Refrigerating and Air-Conditioning Engineers (ASHRAE)](https://www.ashrae.org/)

2. [Building Performance Institute (BPI)](https://www.bpi.org/)

3. [Center for Disease Control and Prevention (CDC)](https://www.cdc.gov/)

4. [Department of Energy (DOE)](https://www.energy.gov/)

5. [Environmental Protection Agency (EPA)](https://www.epa.gov/)

6. [Green & Healthy Homes Initiative (GHHI)](https://www.greenandhealthyhomes.org/)

7. [Green Jobs Career Map](https://greenbuildingscareermap.org/)

8. [Illinois Department of Commerce and Economic Opportunity (DCEO)](https://dceo.illinois.gov/)

9. [National Association for State Community Services Programs (NASCSP)](https://nascsp.org/)

10. [National Center for Healthy Housing (NCHH)](https://nchh.org/)

11. [Occupational Safety and Health Administration (OSHA)](https://www.osha.gov/)

12. [Weatherization Assistance Program Technical Assistance Center (WAPTAC)](https://nascsp.org/wap/waptac/)
