---
title: Home
draft: false
bannerHeading: "Home"
bannerText: >
  Bridging the gaps between energy & health, and between research & practice, to improve real outcomes for residents
---

# Research

Our research helps provide indoor environments that are healthy, comfortable, and efficient

[Explore Research](https://ccrpc.gitlab.io/icrt/research/)

# Training

Our professional trainers have the knowledge to advance the home performance industry

[Find Training](https://ccrpc.gitlab.io/icrt/training/)

<!-- # [Quality Training](https://ccrpc.gitlab.io/icrt/taining/)

# [Indoor Environment Research](https://ccrpc.gitlab.io/icrt/research/)

# [Publications](https://ccrpc.gitlab.io/icrt/publications/) -->

<!-- {{<image src="indoor_research.jpg" alt="Description of the image" attr="Image attribution" position="full" width="500px" height="300px">}} -->
<!--
{{<image src="indoor_research.jpg" alt="Description of the image" attr="[Quality Training](https://ccrpc.gitlab.io/icrt/taining/)" position="full" width="500px" height="300px">}} -->
<!--
{{<image src="indoor_research.jpg" alt="Description of the image" attr="Image attribution <a href='https://example.com'>Link</a>" position="full" width="500px" height="300px">}} -->

<!-- {{<image src="indoor_research.jpg" alt="Description of the image" position="full" width="500px" height="300px">}}

<p><a href="https://ccrpc.gitlab.io/icrt/taining/">Quality Training</a></p>

{{<image src="indoor_research.jpg" alt="Description of the image" position="full" width="500px" height="300px">}}

<p><a href="https://ccrpc.gitlab.io/icrt/research/">Indoor Environment Research</a></p>

{{<image src="publication_button.jpg" alt="Description of the image" position="full" width="500px" height="300px">}}

<p><a href="https://ccrpc.gitlab.io/icrt/publications/">Publications</a></p> -->

<!-- <style>
    .image-link {
        display: inline-block;
        margin-right: 20px;
    }
</style>

{{<image src="indoor_research.jpg" alt="Description of the image" position="full" width="200px" height="150px" class="image-link">}}

<p><a href="https://ccrpc.gitlab.io/icrt/taining/">Quality Training</a></p>

{{<image src="indoor_research.jpg" alt="Description of the image" position="full" width="200px" height="150px" class="image-link">}}

<p><a href="https://ccrpc.gitlab.io/icrt/research/">Indoor Environment Research</a></p>

{{<image src="publication_button.jpg" alt="Description of the image" position="full" width="200px" height="150px" class="image-link">}}

<p><a href="https://ccrpc.gitlab.io/icrt/publications/">Publications</a></p> -->
<!--
_________________________________________________________________________________________________________________________________________ -->

<style>
    .image-container {
        display: flex;
        align-items: center;
    }

    .image-link {
        margin-right: 20px; /* Adjust the margin as needed */
        padding: 0;
    }
</style>

<div class="image-container">
    <div class="image-link">
        {{<image src="quality_taining_picture.jpg" alt="Description of the image" position="full" width="200px" height="150px">}}
        <p><a href="https://ccrpc.gitlab.io/icrt/training/">Quality Training</a></p>
    </div>
    <div class="image-link">
        {{<image src="indoor_research.jpg" alt="Description of the image" position="full" width="200px" height="150px">}}
        <p><a href="https://ccrpc.gitlab.io/icrt/research/">Indoor Environment Research</a></p>
    </div>
    <div class="image-link">
        {{<image src="publication_button.jpg" alt="Description of the image" position="full" width="200px" height="150px">}}
        <p><a href="https://ccrpc.gitlab.io/icrt/publications/">Publications</a></p>
    </div>
</div>

<!-- https://ccrpc.gitlab.io/icrt/research/ -->
