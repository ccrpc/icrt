---
title: "MultiFamily IAQ Study in Chicago"
draft: false
weight: 10
menu: false
summary: " "
bannerHeading: "MultiFamily IAQ Study in Chicago"
---

### Chicago Study for Multifamily Building Ventilation and Indoor Air Quality

### Who is supporting the project?

The Illinois Home Weatherization Assistance Program is providing funding for the project. Indoor Climate Research and Training is partnering with the CEDA Weatherization Program to conduct the project.

### What is the project?

The project is a research study aimed to determine the effectiveness of feasible ventilation system options improvement in large, centrally ventilated multifamily buildings. The research efforts will characterize the impact of weatherization and ventilation on indoor air quality in multifamily buildings, via air-quality measurements in individual dwellings. This will be done by comparing pre-retrofit and post-retrofit data in suitable and selected multi-family buildings in New York State. Multiple indoor contaminants are being measured, including carbon dioxide, nitrogen dioxide, humidity, formaldehyde, and particles.

### Where is the project taking place?

The study aims to test multi-family buildings in the Chicago area.

### When is the project taking place?

The project began in the fall of 2018 and will last through the spring of 2020

### Why are we doing this research?

![test100](chicago-1.jpg)

ICRT is working on this project to help weatherization programs that work in multifamily buildings determine the best approaches to ventilation in order to deliver the best improvements to the building possible without negatively impacting indoor air quality.

Another outcome is to help improve ventilation standards in the context of multi-family buildings.

The primary beneficiary of this pilot study will be the DOE and the weatherization community. Since there is a very limited amount of published data available on ventilation and infiltration rates and their impacts on indoor air quality in large, multifamily buildings, by making results of the pilot study available to public, entities like ASHRAE and other researchers will also benefit.

### How do we conduct the research?

We place indoor air quality sensors in individual apartments for approximately two weeks before and after weatherization. We also evaluate the leakiness of the apartments through testing that involves a large fan, and which is used for a short time on a single day.

Outdoor data collected includes temperature, humidity, carbon dioxide, and particles at the building site.

If you received an informational flyer or brochure about the project and would like to be contacted by one of the researchers for more information about participating in the project please email [icrt@ccrpc.org]().
