---
title: "2019 Publications"
draft: false
weight: 10
menu: false
summary: " "
bannerHeading: "2019 Publications"
---

## 2019

![test200](2019-1.png)

### <span style="color:darkorange">2019 DPI Workshop Summary Report</span>

### <span style="color:darkorange">Report</span>

DPI Workshop Summary Report
Prof. Rich Sowers, Industrial and Enterprise Systems Engineering
Paul W. Francisco, Indoor Climate Research & Training, Champaign County Regional Planning Commission

[DPI workshop summary](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=2997)

![test201](2019-2.jpg)

### <span style="color:darkorange">Unburned Methane Emissions from Residential Natural Gas Appliances</span>

### <span style="color:darkorange">Journal Article </span>

Unburned Methane Emissions from Residential Natural Gas Appliances
Zachary Merrin & Paul W. Francisco
Environmental Science & Technology 53:9, 5473-5482

[Link: https://pubs.acs.org/doi/10.1021/acs.est.8b05323](https://pubs.acs.org/doi/10.1021/acs.est.8b05323)
