---
title: " 2017 Publications"
draft: false
weight: 30
menu: false
summary: " "
bannerHeading: "2017 Publications"
---

## 2017

![test300](17-1.png)

### <span style="color:darkblue">Mechanical Ventilation’s Impact on IAQ</span>

### <span style="color:darkorange">Magazine Article</span>

Mechanical Ventilation’s Impact on IAQ
Paul W. Francisco, David Jacobs, William Rose, Salvatore Cali
ASHRAE Journal 59:5, 96-97
[https://search.proquest.com/openview/6eb323d349d9acb18949c847149058e4/1?pq-origsite=gscholar&cbl=41118](https://www.proquest.com/openview/6eb323d349d9acb18949c847149058e4/1?pq-origsite=gscholar&cbl=41118)
© 2017, ASHRAE. Published in ASHRAE Transactions 2017, Volume 59, Part 5. Reprinted by permission at icrt.appliedresearch.illinois.edu. This article may not be copied and/or distributed electronically or in paper form without permission of ASHRAE. For more information about ASHRAE Transactions, visit www.ashrae.org.
Requests from third parties for use of ASHRAE published content should be directed to www.ashrae.org/permissions.

![test301](17-2.jpg)

### <span style="color:darkblue">Bill Rose’s Building Science To-Do List: Rose invites young building scientists to investigate six topics</span>

### <span style="color:darkorange">Magazine Article</span>

Bill Rose’s Building Science To-Do List: Rose invites young building scientists to investigate six topics
Bill Rose
Green Building Advisor February 2017

[http://www.greenbuildingadvisor.com/articles/dept/musings/bill-rose-s-building-science-do-list](https://www.greenbuildingadvisor.com/article/bill-roses-building-science-to-do-list)

![test302](17-3.jpg)

### <span style="color:darkblue">Ventilation, Indoor Air Quality, and Health in Homes Undergoing Weatherization</span>

### <span style="color:darkorange">Magazine Article</span>

Ventilation, indoor air quality, and health in homes undergoing weatherization
Francisco PW, Jacobs DE, Targos L, Dixon SL, Breysse J, Rose W, Cali S
Indoor Air, 2017 Mar, 27(2): 463-477

[https://www.ncbi.nlm.nih.gov/pubmed/27490066](https://pubmed.ncbi.nlm.nih.gov/27490066/)

![test304](17-4.png)

### <span style="color:darkblue">Transport of Contaminants From Garages Attached or Integral to Low-rise Residential Buildings</span>

### <span style="color:darkorange">Report</span>

Transport of Contaminants From Garages Attached or Integral to Low-rise Residential Buildings
Zachary Merrin; Paul W. Francisco; David Bohac; Josh A. Quinnell; Collin Olson, PhD
Final report for ASHRAE RP-1450

[https://www.techstreet.com/standards/rp-1450-transport-of-contaminants-from-garages-attached-or-integral-to-low-rise-residential-buildings?product_id=1990679](https://www.techstreet.com/standards/rp-1450-transport-of-contaminants-from-garages-attached-or-integral-to-low-rise-residential-buildings?product_id=1990679)

© 2017, ASHRAE. Published in ASHRAE Transactions 2017, RP-1450. Reprinted by permission at icrt.appliedresearch.illinois.edu. This article may not be copied and/or distributed electronically or in paper form without permission of ASHRAE. For more information about ASHRAE Transactions, visit www.ashrae.org.
Requests from third parties for use of ASHRAE published content should be directed to www.ashrae.org/permissions.

![test305](17-5.jpg)

### <span style="color:darkblue">Carbon Monoxide Measurements in Homes</span>

### <span style="color:darkorange">Journal Article</span>

Carbon Monoxide Measurements in Homes
PW Francisco, S Pigg, D Cautley, WB Rose, DE Jacobs, S Cali
Science and Technology for the Built Environment 24:2, 118-123
[Carbon Monoxide Measurements in Homes](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3001)

![test306](17-6.png)

### <span style="color:darkblue">Mapping US Residential Methane Usage</span>

### <span style="color:darkorange">Report</span>

Mapping US Residential Methane Usage
Zachary Merrin, Paul W. Francisco
Prepared for the Environmental Defense Fund
[Mapping US Residential Methane Usage](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3593)

[Appendix B: Mapping US Residential Methane Usage](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3592)
