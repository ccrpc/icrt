---
title: "Mike Hunter"
draft: false
weight: 20
menu: false
summary: "Weatherization Technician and Trainer"
bannerHeading: "Mike Hunter"
---

Weatherization Technician & Trainer
(217) 333-6483
[mhunter@ccrpc.org]()

ICRT - Champaign County Regional Planning Commission

### Biography

I joined ICRT in June of 2022 as a Technical Trainer after 17 years with Champaign County Regional Planning Commission working in the Weatherization field. I have 25 years experience in the construction industry and enjoy working on, modernizing and making older cars and trucks more efficient.

### Other Professional Employment

- Champaign County Regional Planning Commission Urbana, Illinois
- Gemini Construction Ogden, Illinois

### Major Consulting Activities

- Assisted with local agencies Weatherization production by providing instruction , assessment , and inspections.

### Professional Registrations

- All required Weatherization and industry specific certifications to include BPI assessor / final inspector and Lead risk assessor.
