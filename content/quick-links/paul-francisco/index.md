---
title: "Paul Francisco"
draft: false
weight: 10
menu: false
summary: "Director"
bannerHeading: "Paul Francisco"
---

### Primary Research Area

- Monitoring, Diagnostics and Controls, and Training.

### Biography

Paul Francisco is the Senior Coordinator of the Indoor Climate Research & Training (ICRT) group at the Champaign County Regional Planning Commission at Urbana-Champaign. His role is focused on conducting research and directing the ICRT Weatherization Training Center. His research focuses on the intersection of energy efficiency and indoor air quality. His projects are primarily conducted in field environments, and includes indoor air quality sampling, energy use logging, and airflow and insulation characterization of buildings. Partners include other research organizations such as the National Center for Healthy Housing and other field practitioners. On the training side, the ICRT Weatherization Training Center provides training services for the Illinois Home Weatherization Assistance Program which serves thousands of low-income clients each year. The training program provides field practitioners with the tools needed to identify the most cost-effective measures while maintaining health and safety of the residents. Francisco has a BSME from the University of Delaware and an MSME from the University of Washington.

Prior to the creation of the ICRT group in January of 2012, Paul worked for the Building Research Council at the UIUC School of Architecture between 2003 and 2012. In addition to his research and training efforts, Paul is involved in setting national standards and policy regarding ventilation, indoor air quality, and best practices in residential retrofit. He is the current chair of the ASHRAE Standard 62.2 committee which sets national standards for residential ventilation and was a former chair of ASHRAE’s Environmental Health Committee. Mr. Francisco is a regular speaker at national and international conferences on both science and practice and has delivered over 30 presentations in the last two years.
