---
title: " 2014 Publications"
draft: false
weight: 60
menu: false
summary: " "
bannerHeading: "2014 Publications"
---

## 2014

### <span style="color:darkorange">Report</span>

#### Weatherization and Indoor Air Quality: Measured Impacts in Single-Family Homes under the Weatherization Assistance Program

Pigg, S., D. Cautley, P. Francisco, B. Hawkins, and T. Brennan
Prepared for the U.S. Department of Energy under contract DE-AC05-00OR22725

### <span style="color:darkorange">Conference Proceeding</span>

#### ASHRAE Standard 62.2: What’s New and Why

P.W. Francisco
Proceedings of Indoor Air 2014, Hong Kong, July 2014

### <span style="color:darkorange">Conference Proceeding</span>

#### Radon Reduction through Floor Air Sealing

Rose, W.B., P.W. Francisco, and Z. Merrin
Proceedings of Indoor Air 2014, Hong Kong, July 2014

#### Weatherization and Indoor Air Quality: Measured Impacts in Single-family Homes under the Weatherization Assistance Program

Scott Pigg, Dan Cautley, Paul Francisco, Beth Hawkins, Terry Brennan
ORNL/TM-2014/170
September 2014

https://weatherization.ornl.gov/Retrospectivepdfs/ORNL_TM-2014_170.pdf
