---
title: "VaShawn Johnson"
draft: false
weight: 20
menu: false
summary: "Weatherization Technician and Trainer"
bannerHeading: "VaShawn Johnson"
---

### Weatherization Technician & Trainer

(217) 333-6458
[vjohnson@ccrpc.org]()

ICRT - Champaign County Regional Planning Commission
