---
title: "Kiel Gilleade"
draft: false
weight: 20
menu: false
summary: "Research Engineer"
bannerHeading: "Kiel Gilleade"
---

### Research Areas

- Monitoring, Diagnostics and Controls, and Training
