---
title: " 2016 Publications"
draft: false
weight: 40
menu: false
summary: " "
bannerHeading: "2016 Publications"
---

## 2016

![test401](16-1.jpg)

### <span style="color:darkblue">Duct Leakage Repeatability Testing</span>

### <span style="color:darkorange">Conference Proceeding</span>

Duct Leakage Repeatability Testing
Walker, M. Sherman, and P.W. Francisco
Proceedings of the Performance of Exterior Envelopes of Whole Buildings XIII Conference in Clearwater, FL.

![test402](16-2.jpg)

### <span style="color:darkblue">Practical Applications and Case Study of Temperature Smart Ventilation Controls</span>

### <span style="color:darkorange">Conference Proceeding</span>

### <span style="color:darkorange">Performance of Exterior Envelopes of Whole Buildings XII</span>

Practical Applications and Case Study of Temperature Smart Ventilation Controls
Lubliner, P. Francisco, E. Martin, I. Walker, B. Less, R. Vieira, R. Kunkle, and Z. Merrin
Proceedings of the Performance of Exterior Envelopes of Whole Buildings XIII Conference in Clearwater, FL.
[Practical Applications and Case Study of Temperature-Based Smart Ventilation Controls](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=2999)

![test403](16-3.png)

### <span style="color:darkblue">The Latest Developments in Residential Combustion Safety Testing</span>

### <span style="color:darkorange">Conference Proceeding</span>

### <span style="color:darkorange">ASHRAE IAQ 2016</span>

The Latest Developments in Residential Combustion Safety Testing
P.W. Francisco, L. Brand, D. Cautley, B. Singer, and S. Gloss
Proceedings of the ASHRAE IAQ 2016 Conference, Alexandria, VA, Sept. 2016
[The Latest Developments in Residential Combustion Safety Testing](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3000)

![test404](16-4.png)

### <span style="color:darkblue">Carbon Monoxide Measurements in Homes</span>

### <span style="color:darkorange">Conference Proceeding</span>

Carbon Monoxide Measurements in Homes
P.W. Francisco, S. Pigg, D. Cautley, W.B. Rose, D. Jacobs, and S. Calie
Proceedings of the ASHRAE IAQ2016 Conference, Alexandria, VA, Sept. 2016
[Carbon Monoxide Measurements in Homes](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3001)

![test405](16-5.png)

### <span style="color:darkblue">Attached Garages: IAQ Implications and Solutions</span>

### <span style="color:darkorange">Conference Proceeding</span>

ASHRAE IAQ 2016
Attached Garages: IAQ implications and solutions
Merrin, P.W. Francisco, D. Bohac, J.A. Quinnell, and C. Olson.
Proceedings of the ASHRAE IAQ2016 Conference, Alexandria, VA, Sept. 2016
[Attached Garages IAQ Implications and Solutions](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3002)

![test406](16-6.png)

### <span style="color:darkblue">Addressing Residential</span>

### <span style="color:darkorange">Magazine Article</span>

Addressing Residential
Paul W. Francisco, Max H. Sherman
ASHRAE Journal, 58:5, 84-86
[https://search.proquest.com/openview/e33d4008c095c42ba7820f882dadc165/1?pq-origsite=gscholar&cbl=41118](https://www.proquest.com/openview/e33d4008c095c42ba7820f882dadc165/1?pq-origsite=gscholar&cbl=41118)
© 2016, ASHRAE. Published in ASHRAE Transactions 2016, Volume 58, Part 5. Reprinted by permission at icrt.appliedresearch.illinois.edu. This article may not be copied and/or distributed electronically or in paper form without permission of ASHRAE. For more information about ASHRAE Transactions, visit www.ashrae.org.
Requests from third parties for use of ASHRAE published content should be directed to www.ashrae.org/permissions.

![test407](16-7.jpg)

### <span style="color:darkblue">Air Quality Implications of Heating with Wood Stoves</span>

### <span style="color:darkorange">Conference Proceeding</span>

Air Quality Implications of Heating with Wood Stoves
Zachary Merrin, Paul W. Francisco, Tami C. Bond, Nicholas L. Lam, Ryan Thompson, Cheryl Weyant
Indoor Air 2016, Conference proceeding
[https://www.isiaq.org/docs/Papers/Paper989.pdf](https://www.isiaq.org/docs/Papers/Paper989.pdf)

![test408](16-8.jpg)

### <span style="color:darkblue">Residential Natural Gas Emissions from Pipe Leaks and Heating Appliances</span>

### <span style="color:darkorange">Conference Proceeding</span>

Residential Natural Gas Emissions from Pipe Leaks and Heating Appliances
Zachary Merrin, Stacy L. Gloss, Paul W. Francisco
Indoor Air 2016, Conference proceeding
[https://www.isiaq.org/docs/Papers/Paper987.pdf](https://www.isiaq.org/docs/Papers/Paper987.pdf)

![test409](16-9.jpg)

### <span style="color:darkblue">Characterizing Indoor Humidity for Comparison Studies: the Moisture Balance Approach</span>

### <span style="color:darkorange">Conference Proceeding</span>

Characterizing Indoor Humidity for Comparison Studies: the Moisture Balance Approach
Rose W, Gloss S
Buildings XIII
[https://web.ornl.gov/sci/buildings/2016/docs/presentations/principles/principles-06/Principles06_Paper120_Rose.pdf](https://web.ornl.gov/sci/buildings/2016/docs/presentations/principles/principles-06/Principles06_Paper120_Rose.pdf)
