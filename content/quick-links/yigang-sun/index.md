---
title: "Yigang Sun"
draft: false
weight: 20
menu: false
summary: "Senior Research Scientist"
bannerHeading: "Yigang Sun"
---

### Primary Research Area

- Monitoring, Diagnostics and Controls, and Training

### Biography

Dr. Yigang Sun is a Research Engineer with the ICRT group at the Champaign County Regional Planning Commission. He specializes in measuring, simulating, and controlling indoor airflows and air quality. His current research interests include residential building indoor air quality, indoor climate comfort, ventilation, heating and cooling, and energy efficiency. Before joining the ICRT, Dr. Sun worked as a senior research engineer in the Department of Agricultural and Biological Engineering at the University of Illinois at Urbana-Champaign, with a focus on indoor air ventilation and air quality, and bio-energy technology.

Dr. Sun earned his Ph.D. in Agricultural and Biological Engineering from UIUC in 2007. His thesis work developed a volumetric particle-streak tracking velocimetry system to measure and visualize whole-field indoor airflow in rooms non-intrusively and simultaneously.
