---
title: "Gary Duden"
draft: false
weight: 20
menu: false
summary: "Weatherization Technician and Trainer"
bannerHeading: "Gary Duden"
---

Weatherization Technician & Trainer
(217) 333-9266
[gduden@ccrpc.org]()

ICRT - Champaign County Regional Planning Commission

### Education

B.A. Speech Communication Eastern Illinois University 1998

### Academic Positions

Weatherization Technical Trainer University of Illinois July 2022

### Professional Registrations

N.A.T.E. Certified
