---
title: "2018 Publications"
draft: false
weight: 20
menu: false
summary: " "
bannerHeading: "2018 Publications"
---

## 2018

![test203](2018-1.jpg)

### <span style="color:darkblue">An Evaluation of Strategies to Reduce Transport of Pollutants from Garages to Homes</span>

### <span style="color:darkorange">Journal Article</span>

An evaluation of strategies to reduce transport of pollutants from garages to homes
Zachary Merrin, Paul W. Francisco, David Bohac, Josh A. Quinnell & Collin Olson
Science and Technology for the Built Environment, 24:2, 198-208

[https://doi.org/10.1080/23744731.2017.1417664](https://www.tandfonline.com/doi/full/10.1080/23744731.2017.1417664)

![test204](2018-2.jpg)

### <span style="color:darkblue">Impacts of Weatherization on Indoor Air Quality</span>

### <span style="color:darkorange">Journal Article</span>

Impacts of weatherization on indoor air quality: A field study of 514 homes
S. Pigg, D. Cautley, P.W. Francisco
Indoor Air, 2018 Mar, 28(2): 307-317
DOI: 10.1111/ina.12438

[https://www.ncbi.nlm.nih.gov/pubmed/29108089](https://pubmed.ncbi.nlm.nih.gov/29108089/)
