---
title: "New Home Indoor Air Quality Studies"
draft: false
weight: 20
menu: false
summary: " "
bannerHeading: "New Home Indoor Air Quality Studies"
---

ICRT is conducting two separate indoor air quality studies on modern homes (2010 & newer) in Central Illinois. Both studies are funded by the US Department of Energy in partnership with Lawrence Berkeley National Laboratory (LBNL). Participants will be compensated with a home-improvement store gift card. Some homes may be eligible to participate in both studies.

### Week Intensive Indoor Air Quality Study

<!-- ![test5](new_indoor-1.jpg) -->
<img src="new_indoor-1.jpg" alt="Description of the image" width="300" height="350">

Measuring ventilation air flow

<!-- ![test6](new_indoor-2.jpg) -->
<img src="new_indoor-2.jpg" alt="Description of the image" width="300" height="350">

Outdoor monitoring station

### What does participation involve?

ICRT seeks newer (built in 2013 or later) single-family homes to participate in an intensive indoor air quality research study. Participation involves 2-3 ICRT staff members visiting your home and placing temporary monitors to measure air quality indoors and outdoors, and monitor appliance operation. The installation visit may last 6-8 hours, and the monitors will remain for one week. A second visit of 6-8 hours will take place at the end of the data collection week to retrieve monitors and conduct air flow testing. We ask that participants keep their windows closed during the week, and log activities that may affect indoor air quality, such as house cleaning. Participants will also be asked to complete a survey to describe the house and occupancy characteristics.

### Why should I sign up?

Collected data will inform researchers about how indoor air quality varies according to factors such as geographic location and home characteristics. Participants will receive compensation as $300 home improvement store gift card, and a report summarizing results for your home.

![test7](new_indoor-3.jpg)

<!-- Outdoor air quality measurements of particulate matter (PM), nitrogen dioxide, and formaldehyde. -->

<!-- ![test8](new_indoor-4.jpg)
Intdoor air quality measurements of particulate matter (PM), carbon dioxide, nitrogen dioxide, and formaldehyde in main living space.

![test9](new_indoor-5.jpg)
Intdoor air quality measurements of carbon dioxide, and formaldehyde in master bedroom. -->

### Does my home qualify for the study?

Homes that qualify for the study are single-family detached or attached houses (townhouses), built in 2013 or later, and located in Central or Northern Illinois. Homes with indoor smoking are ineligible. Some homes will qualify for a second week of air quality monitoring with some changes to the home ventilation strategy (such as keeping fans on for a longer period of time). Homes that complete the second week of study will receive an additional $200 home improvement store gift card.

### How do I sign up?

Contact Kiel at [ICRT Radon Study]() or call (217) 300-4485 to sign up or learn more. We look forward to hearing from you!

Potential participants will set up an eligibility screening call to review study procedures, and answer questions about existing home appliances and ventilation appliances (up to 30 minutes).

### 6 Week Radon Study in Central / East Central Illinois

ICRT seeks newer (built in 2010 or later) single family homes for a research study comparing radon levels in homes that have a passive radon vent pipe (no fan installed) to levels in homes without a radon mitigation system.

Eligible homes will be:

1. Single family detached houses (owned or with landlords permission)
2. Located in Central or East-Central Illinois
3. Built in 2010 or later
4. With either:
   1. A Radon-Resistant New Construction (RRNC) system – passive radon vent pipe without a fan installed
   2. No radon mitigation system

### What does participation involve?

What is Involved for Radon Research Participants?

The project approach is as follows:

- ICRT will test the study homes for radon for six weeks. At a minimum, ICRT needs a pair of homes (a home with a radon pipe and one with no radon pipe) tested at the same time. We are recruiting 11 pairs of homes for the study for this summer.
- An ICRT researcher will deploy continuous radon monitors for this period, one in the foundation space and one on the first floor.
- For RRNC homes, the ICRT researcher will cut through the radon pipe near the base (typically in a basement), and insert a metal disk which will then be sealed by tape. The resident will be asked to alternate removing/replacing the disk once a week so that we get six weeks of flip-flop.
- At the conclusion, ICRT returns to install a permanent seal of the cut using Illinois Emergency Management Agency (IEMA) Radon Office-approved methods (repair coupling with suitable sealant, i.e. caulk or cement depending on whether the pipe is PVC or ABS).
- At the conclusion, ICRT will conduct a blower door test on each home to evaluate home air infiltration and remove the radon monitors.
- Each visit should be about 45-60 minutes for homes with a radon pipe present and about 30 minutes homes without a radon pipe present. The final visit may take up to 90 minutes.
- We request windows to be closed for the full six weeks.
- Residents will receive a report of radon levels in their homes.

Participants whose home has the radon pipe will receive a $100 home improvement store gift card.

Participants without radon controls will receive a $50 home improvement store gift card.

### How do I sign up?

[Contact Kiel at ICRT Radon Study]() or call +1 (217) 300-4485 to sign up or learn more.

A researcher will set up a call with you to describe the study and answer questions.

Eligibility Forms:

[Primary Consent Form non-RRNC home](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3110)

### [Primary Consent Form RRNC home](https://ws.engr.illinois.edu/sitemanager/getfile.asp?id=3111)

### Meet the Project Team

![test10](research-1.jpg)

[Paul Francisco](https://ccrpc.gitlab.io/icrt/quick-links/paul-francisco/),
Director

<!-- Primary Research Area - Monitoring, Diagnostics and Controls

Biography

Paul Francisco is the Senior Coordinator of the Indoor Climate Research & Training (ICRT) group at the Illinois Applied Research Institute at the University of Illinois at Urbana-Champaign. His role is focused on conducting research and directing the ICRT Weatherization Training Center. His research focuses on the intersection of energy efficiency and indoor air quality. His projects are primarily conducted in field environments, and includes indoor air quality sampling, energy use logging, and airflow and insulation characterization of buildings. Partners include other research organizations such as the National Center for Healthy Housing and other field practitioners. On the training side, the ICRT Weatherization Training Center provides training services for the Illinois Home Weatherization Assistance Program which serves thousands of low-income clients each year. The training program provides field practitioners with the tools needed to identify the most cost-effective measures while maintaining health and safety of the residents. Francisco has a BSME from the University of Delaware and an MSME from the University of Washington.
Prior to the creation of the ICRT group in January of 2012, Paul worked for the Building Research Council at the UIUC School of Architecture between 2003 and 2012. In addition to his research and training efforts, Paul is involved in setting national standards and policy regarding ventilation, indoor air quality, and best practices in residential retrofit. He is the current chair of the ASHRAE Standard 62.2 committee which sets national standards for residential ventilation and was a former chair of ASHRAE’s Environmental Health Committee. Mr. Francisco is a regular speaker at national and international conferences on both science and practice and has delivered over 30 presentations in the last two years. -->

![test11](research-2.jpg)

[Kiel Gilleade](https://ccrpc.gitlab.io/icrt/quick-links/kiel-gilleade/),
Research Engineer

<!--
Research - Monitoring, Diagnostics and Controls, Training -->

![test12](research-3.jpg)

[Yigang Sun](https://ccrpc.gitlab.io/icrt/quick-links/yigang-sun/),
Senior Research Scientist

<!--
Primary Research Area - Monitoring, Diagnostics and Controls, and Training

Biography

Dr. Yigang Sun is a Research Engineer with the ICRT group at the Illinois Applied Research Institute. He specializes in measuring, simulating, and controlling indoor airflows and air quality. His current research interests include residential building indoor air quality, indoor climate comfort, ventilation, heating and cooling, and energy efficiency. Before joining the ARI, Dr. Sun worked as a senior research engineer in the Department of Agricultural and Biological Engineering at the University of Illinois at Urbana-Champaign, with a focus on indoor air ventilation and air quality, and bio-energy technology.
Dr. Sun earned his Ph.D. in Agricultural and Biological Engineering from UIUC in 2007. His thesis work developed a volumetric particle-streak tracking velocimetry system to measure and visualize whole-field indoor airflow in rooms non-intrusively and simultaneously. -->
