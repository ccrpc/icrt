---
title: "Lori Shupe"
draft: false
weight: 20
menu: false
summary: "Education, Research, and Training Manager"
bannerHeading: "Lori Shupe"
---

### Primary Research Area

- Building Science, and Training

### Biography

Lori Shupe, EdM, has been at Indoor Climate Research & Training since January 2013. She currently manages the training center, working with weatherization instructors and the State Weatherization Manager to address the training needs of those working in the Illinois Home Weatherization Assistance Program. Her role is focused on seeking ways to improve both processes and programs under the purview of ICRT.

Lori is a believer in continuous learning and self-improvement and has a variety of professional interests. She is a member of the American Society for Training and Development, the Society for Human Resource Management, and the American Management Association.
