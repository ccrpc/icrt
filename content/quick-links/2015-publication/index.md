---
title: " 2015 Publications"
draft: false
weight: 50
menu: false
summary: " "
bannerHeading: "2015 Publications"
---

## 2015

#### Low-Cost Radon Reduction Pilot Study

William B. Rose, Paul W. Francisco, and Zachary Merrin
US Department of Energy, Building America, Partnership for Advanced Residential Retrofit, Report

https://www1.eere.energy.gov/buildings/publications/pdfs/building_america/low-cost-radon-reduction-pilot-study.pdf

#### WEATHERIZATION VENTILATION STRATEGY AS A FACTOR IN INDOOR HUMIDITY

William ROSE, Stacy GLOSS, Paul FRANCISCO, Zachary MERRIN, Salvatore CALI, and David JACOBS
Healthy Buildings 2015, Conference proceeding

https://www.isiaq.org/docs/Proceedings_HB2015-America.pdf#page=279

#### Technology Solutions Case Study: Combustion Safety Simplified Test Protocol Field Study

L Brand, D Cautley, D Bohac, P Francisco, L Shen, and S Gloss
Partnership for Advanced Residential Retrofit and NorthernSTAR
USDOE November 2015

https://www.osti.gov/biblio/1233566

### <span style="color:darkorange">Conference Proceeding</span>

#### Low Cost Radon Reduction Pilot Study: Air-tightening at Foundation-house Interface as a Means of Radon Reduction in Living Space

Rose, W., Z. Merrin, P. Francisco, and S. Gloss
Proceedings of Healthy Buildings America, 2015, Boulder Co.

https://www.isiaq.org/docs/Proceedings_HB2015-America.pdf#page=536

### <span style="color:darkorange">Report</span>

#### Low Cost Radon Reduction Pilot Study

Rose, W., P. Francisco, Z. Merrin
Prepared for the U.S. Department of Energy/National Renewable Energy Laboratory under subcontract KNDJ-0-40346-04

https://www1.eere.energy.gov/buildings/publications/pdfs/building_america/low-cost-radon-reduction-pilot-study.pdf

### <span style="color:darkorange">Conference Proceeding</span>

#### Weatherization ventilation strategy as factor in indoor humidity

Rose, W., S. Gloss, P. Francisco, Z. Merrin, S. Cali, and D. Jacobs
Proceedings of Healthy Buildings America, 2015, Boulder Co.

https://www.isiaq.org/docs/Proceedings_HB2015-America.pdf#page=279

### <span style="color:darkorange">Report</span>

#### Development of Test Methodology and Initial Pilot Field Testing for Residential Methane Emissions Quantification

P.W. Francisco, Z. Merrin, and S. Gloss
Prepared for the Environmental Defense Fund

### <span style="color:darkorange"> Report</span>

#### Combustion Safety Simplified Test Protocol Field Study

Brand, L., D. Cautley, D. Bohac, P. Francisco, L. Shen, and S. Gloss
Prepared for the U.S. Department of Energy/National Renewable Energy Laboratory under subcontract KNDJ-0-40346-05

### <span style="color:darkorange"> Report</span>

#### Ventilation, Indoor Air Quality and Health Outcomes in Home Weatherization

Francisco, P.W., D.E. Jacobs, L. Targos, S.L. Dixon, J. Breysse, B. Rose, H. Dawson, S. Nall, D. Phillips, and S. Cali
Prepared for the U.S. Department of Housing and Urban Development, Grant Number ILLHH0230-10
