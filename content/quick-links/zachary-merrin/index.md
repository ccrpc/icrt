---
title: "Zachary Merrin"
draft: false
weight: 20
menu: false
summary: "Research Engineer"
bannerHeading: "Zachary Merrin"
---

### Primary Research Area

- Building Science, Monitoring, Diagnostics and Controls, and Training

### Biography

Zachary Merrin is a Research Engineer in the field of building science with the Champaign County Regional Planning Commission’s Indoor Climate Research and Training program.

Zachary specializes in Indoor Air Quality and Energy Efficiency within residential applications. His recent research topics include contaminant transport from attached garages, climatic and indoor air quality implications of biomass combustion for cooking and heating, non-biological methane emissions from residential end users, changes in building performance related to weatherization retrofits, and investigations into home performance best practices. He is an experienced field researcher and project manager having completed multiple research campaigns in challenging locations both domestically and internationally.

Zachary currently serves on the Building Performance Institute’s Single Family Standards Technical Committee. His previous experience includes industrial emissions sampling with CleanAir Engineering and industrial/commercial process efficiency evaluation with the Illinois Environmental Protection Agency. Zach earned his B.S. degree in Industrial and Enterprise Systems Engineering, and his M.S. degree in Environmental Engineering, both from the University of Illinois Urbana-Champaign.
