---
title: "About Weatherization"
draft: false
weight: 20
menu: false
summary: " "
bannerHeading: "About Weatherization"
---

The Illinois Home Weatherization Assistance Program (IHWAP) helps low income residents and households conserve fuel and reduce energy costs by making their homes and apartments more energy efficient. IHWAP also provides many health and safety upgrades ensuring safe and healthy homes. Energy conservation and health and safety measures provided through home weatherization can ensure citizens have access to more affordable energy services. Weatherization services that can be funded through IHWAP include:

- Air sealing
- Attic and wall insulation
- HVAC repair or replacement
- Water heater repair or replacement
- Electric base load reduction (lighting and refrigerator replacement)
- Ventilation and moisture control measures (and other health and safety measures)
- Maximum $16,000 per eligible client’s home for energy-related weatherization and repair work
- Maximum $3,500 for health and safety related measures

Eligibility depends on household income (income of everyone living in the unit). To be eligible to receive - assistance, the household’s combined income must be at or below 150% of the federal poverty level using State funds, and 200% of the federal poverty level using DOE and HHS funding.

Find a [Community Action Agency (CAA)](https://dceo.illinois.gov/) serving your area and apply for assistance.
[Program Eligibility](https://dceo.illinois.gov/communityservices/homeweatherization/howtoapply.html)

For more information contact:

Office of Community Assistance
Illinois Department of Commerce
217.785.2533
[communityassist@illinois.gov]()
