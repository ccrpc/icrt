---
title: "Training Home"
draft: false
weight: 10
summary: " "
bannerHeading: "Training"
---

Spreading knowledge to advance the home performance industry

ICRT was one of the first training centers accredited by the Interstate Renewable Energy Council [(IREC)](https://irecusa.org/) to deliver quality building science training. The program trains and certifies assessors, inspectors, and other associates from Illinois community action agencies to perform weatherization work such as energy audits, quality control inspections, insulation, air sealing, HVAC inspection and improvements on qualifying residences.

![ICRT'S team picture](icrt_team_picture.jpg)

### IHWAP Certifications

### Energy Auditor

1. Weatherization Basics
2. Heat Transfer
3. Building Fundamentals
4. Introduction to Building Diagnostics
5. Infrared Thermography
6. Mid-Course Field Session
7. Introduction to Heating Systems
8. Advanced Heating Systems
9. Air-conditioning/Heat Pumps
10. Health & Safety, Indoor Pollutants, Lead-safe Weatherization
11. Proficiency Test
12. Weatherization Building Assessment

### Quality Control Inspector

1. Successful completion of the Energy Auditor certification
2. Quality Control Inspections

### Continuing Education

1. Weatherization Program Updates
2. ASHRAE 62.2
3. Workshops:
   - Housing types and air sealing
   - Basic Introduction to Electricity for Weatherization
   - Mechanical Systems Troubleshooting and Installing 90% Furnaces
   - Lead-Safe Weatherization
   - Mobile Home
   - Confined Spaces

### Contractor Training

Architectural Certification Training
HVAC Certification Training
Insulation Best Practices and Certification
Insulation Certification Testing
