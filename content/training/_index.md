---
title: "Training"
weight: 40
menu: main
summary: "ICRT provides certified assessors, inspectors, and associates for Illinois community action agencies, including weatherization, energy audits, insulation, HVAC inspections, and promoting sustainable living."
bannerHeading: "Training"
---

Join us in advancing your skills and contributing to the home performance industry through ICRT's accredited training. As pioneers accredited by the Interstate Renewable Energy Council (IREC), we offer high-quality building science training for assessors, inspectors, and associates from Illinois community action agencies. Gain expertise in weatherization tasks such as energy audits, quality control inspections, insulation, air sealing, and HVAC improvements on qualifying residences.

Earn valuable IHWAP Certifications, including Energy Auditor, Quality Control Inspector, Contractor Training, and more. Take the next step in your career and make a positive impact on the communities we serve. Explore the opportunities for growth and certification below or navigate through the top menu to learn more about our training initiatives. Start your journey with ICRT today!