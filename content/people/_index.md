---
title: "People"
draft: false
weight: 25
menu: main
summary: "Information on the ICRT personnel"
bannerHeading: "People"
---

ICRT is comprised of dedicated and experienced project staff and trainers who bring a wealth of expertise to our initiatives. Our team includes an Education, Research, & Training Manager, Weatherization Technician & Trainer, Research Engineer, and research scientists. Explore the profiles of our talented team members by clicking the links below or navigating through the top navigation section to learn more about their contributions and expertise