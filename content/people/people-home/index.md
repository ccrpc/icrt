---
title: "People Home"
draft: false
weight: 10
summary: " "
bannerHeading: "People"
---

## Leadership

<style>
    .image-container-1 {
        display: flex;
        align-items: center;
    }

    .image-link-1 {
        margin-right: 50px; 
        padding: 0;
    }
</style>

<div class="image-container-1">
    <div class="image-link-1">
        <img src="people___1.jpg" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/paul-francisco/">Paul Francisco</a></p>
        <p>Director</p>
    </div>
    <div class="image-link-1">
        <img src="people___2.jpg" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/lori-shupe/">Lori Shupe</a></p>
        <p>Education, Research, & Training Manager</p>
    </div>
</div>

## Training Staff

<div class="image-container-1">
    <div class="image-link-1">
        <img src="people___3.jpg" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/paul-francisco/">Paul Francisco</a></p>
        <p>Director</p>
    </div>
    <div class="image-link-1">
        <img src="people___10.jpg" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/lori-shupe/">Lori Shupe</a></p>
        <p>Education, Research, & Training Manager</p>
    </div>
</div>

<div class="image-container-1">
<div class="image-link-1">
        <img src="people___5.png" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/kristopher-chapman/">Kristopher Chapman</a></p>
        <p>Program Manager</p>
    </div>
    <div class="image-link-1">
        <img src="people___6.png" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/gary-duden/">Gary Duden</a></p>
        <p>Weatherization Technician & Trainer</p>
    </div>
</div>

<div class="image-container-1">
    <div class="image-link-1">
        <img src="people___7.png" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/mike-hunter/">Mike Hunter</a></p>
        <p>Weatherization Technician & Trainer</p>
    </div>
    <div class="image-link-1">
        <img src="people___8.png" alt="Description of the image" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/vashawn-johnson/">VaShawn Johnson</a></p>
        <p>Weatherization Technician & Trainer</p>
    </div>
</div>

<div class="image-container-1">
    <div class="image-link-1">
        <img src="people___9.jpg" alt="Description of the image" width="260" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/nate-price/">Nate Price</a></p>
        <p>Weatherization Technician & Trainer</p>
    </div>
</div>

## Project Staff

<style>
    .image-container-2 {
        display: flex;
        align-items: center;
    }

    .image-link-2 {
        margin-right: 50px; 
        padding: 0;
    }
</style>

<div class="image-container-2">
    <div class="image-link-2">
        <img src="people___11.jpg" alt="Description of the image" position="full" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/paul-francisco/">Paul Francisco</a></p>
        <p>Director</p>
    </div>
    <div class="image-link-2">
        <img src="people___12.jpg" alt="Description of the image" position="full" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/kiel-gilleade/">Kiel Gilleade</a></p>
        <p>Research Engineer</p>
    </div>
</div>
<div class="image-container-2">
<div class="image-link-2">
        <img src="people___13.jpg" alt="Description of the image" position="full" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/zachary-merrin/">Zachary Merrin</a></p>
        <p>Senior Research Engineer</p>
    </div>
    <div class="image-link-2">
        <img src="people___14.jpg" alt="Description of the image" position="full" width="250" height="374">
        <p><a href="https://ccrpc.gitlab.io/icrt/quick-links/yigang-sun/">Yigang Sun</a></p>
        <p>Senior Research Scientist</p>
    </div>
</div> 
<!-- 
<img src="people___14.jpg" alt="Description of the image" width="260" height="374">

<!-- [Yigang Sun](https://ccrpc.gitlab.io/icrt/quick-links/yigang-sun/)

Senior Research Scientist -->
