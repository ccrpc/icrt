---
title: "Affiliate Trainers"
draft: false
weight: 20
summary: " "
bannerHeading: "Affiliate Trainers"
---

<!-- ![test501](people-1.jpg) -->
<!-- <img src="people-1.jpg" alt="Description of the image" width="250" height="300"> -->
<img src="people-1.jpg" alt="Description of the image" width="250">

#### Randy Bennett

Randy Bennett has worked in the Weatherization/Insulation Industry for 42 years. Randy retired from the State of Illinois in 2012 after 32 years in Weatherization, serving as State Weatherization Manager for 12 years. He was responsible for preparing Department of Energy State Plans, training, writing policy, reports, and manuals, and overseeing the IHWAP program operations. Prior to state service, Mr. Bennett was a Weatherization Coordinator at a Central Illinois Community Action Agency and also spent several years as an insulation contractor. Randy and his wife, Cecilia designed and built an award-winning super-insulated earth-sheltered home in Petersburg, IL. At ICRT, Randy teaches classes on Weatherization Basics, Weatherization 102, Energy Auditor Certification, and Quality Control Inspector.

<hr>

<!-- ![test502](people-2.jpg) -->
<!-- <img src="people-2.jpg" alt="Description of the image" width="250" height="300"> -->
<img src="people-2.jpg" alt="Description of the image" width="250">

#### Jim Cavallo

Jim Cavallo is the managing director of Midwest Energy Performance Analytics, Inc., which provides consultation and testing on energy efficiency opportunities in residential and commercial buildings. He has been an associate editor of Home Energy Magazine and was the founding executive director of the Illinois Association of Energy Raters and Home Performance Professionals. Jim currently teaches weatherization staff and contractors in the Illinois weatherization certification program. Prior to starting his own firm in 2000, Jim was on the research staff of Argonne National Laboratory where he directed a program on energy efficiency in existing buildings. He holds a Ph.D. from New York University in economics with a focus on environmental economics and policy. Jim also volunteers building system and energy efficiency consulting services to congregations of all faiths through Faith in Place, a Chicago-based non-profit assisting congregations in their stewardship of the environment.

<hr>

<!-- ![test503](people-3.jpg) -->
<!-- <img src="people-3.jpg" alt="Description of the image" width="250" height="300"> -->
<img src="people-3.jpg" alt="Description of the image" width="250">

#### Paul A. Knight

Paul Knight has dedicated the vast majority of his 40-year career as a licensed architect to creating healthy, sustainable, and energy-efficient housing. He frequently provides technical and architectural consulting services to federal, state, and city government agencies working to improve the energy efficiency of affordable housing. Paul founded Domus PLUS, a residential energy-efficiency consulting firm, in 1994. Paul’s significant accomplishments through Domus PLUS include:
Incorporating energy-efficient and sustainable building techniques into the new development and rehabilitation of affordable housing, as a technical consultant for the Affordable Housing New Construction Program, sponsored by ComEd (formerly sponsored by the Illinois Department of Commerce and Economic Opportunity); participating since the program’s inception in the rehabilitation of 4,674 multi-family units, the construction of 7,729 new multi-family units, and the development of 1,944 new single-family homes.
Developing weatherization standards for the Illinois Office of Energy Assistance’s Home Weatherization Assistance Program; creating architectural, mechanical, and health and safety standards, including diagnostic testing standards for air leakage, duct leakage, zone pressure and combustion safety testing.
Providing training in the Illinois Home Weatherization Assistance Program; classes include Building Fundamentals, Building Assessment, Crew Leader certification and Contractor Blower Door and Air Sealing workshops.
Providing mold, moisture, and energy assessments to Native American Housing Authorities, through the Building Research Council of the University of Illinois at Urbana-Champaign; conducting more than 30 site inspections; making recommendations to improve energy efficiency and solve moisture problems.
Prior to founding Domus PLUS, Paul served as a research architect at the Energy Resources Center at the University of Illinois at Chicago. He was involved with providing residential technical assistance during his sixteen years at the Center.
Paul holds a master’s degree in architecture from the University of Illinois at Champaign-Urbana. He earned a bachelor’s degree in environmental design from Miami University.

<hr>
<!-- ![test504](people-4.jpg) -->
<!-- <img src="people-4.jpg" alt="Description of the image" width="250" height="300"> -->
<img src="people-4.jpg" alt="Description of the image" width="250">

#### Bob Murphy

Bob Murphy spent 22 years in the HVAC industry, operating his own shop for 11 of those years. Bob has 14 years of experience in weatherization and spent 12 of those years as the weatherization coordinator for Crosswalk Community Action Agency in West Frankfort, Illinois.

<hr>

<!-- ![test505](people-5.jpg) -->
<!-- <img src="people-5.jpg" alt="Description of the image" width="250" height="300"> -->
<img src="people-5.jpg" alt="Description of the image" width="250">

#### Richard White

Richard White is a retired Professor at Lincoln Land Community College in Springfield, IL. He teaches the HVAC program at the College and is a contract trainer for the Illinois Weatherization Program through the University of Illinois Champaign. Richard develops short term training courses that focus on hands on training for the Heating and Cooling industry. Before teaching Richard was an HVAC/Electrical contractor for 20 years. He has BA from Northwestern Medical School, an MS in Vocational Education from Southern Illinois University and ABD (All but dissertation) Doctorate from Illinois State University in the field of Curriculum and Instruction.
