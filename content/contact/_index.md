---
title: "Contact"
weight: 80
menu: main
summary: "Contact infromation of ICRT"
bannerHeading: "Contact"
---

### You’ve learned more about what Indoor Climate Research and Training does, and now want to get in touch.

For general inquiries, send an email to [icrt@ccrpc.org]()

Meeting us at our Champaign training facility?

We are located at 2111 South Oak Street, Suite 106 Champaign, IL 61820.

<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.4065272878515!2d-88.24356078598079!3d40.08864828345365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880cd6d5d5a6fdc9%3A0x8d72d846343cd00b!2s2111+S+Oak+St%2C+Champaign%2C+IL+61820!5e0!3m2!1sen!2sus!4v1526678296610" width="400" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
