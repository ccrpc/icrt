Import-Module Microsoft.PowerShell.Archive

$themeDir = "themes/plandocument"
$themeVersion = "0.1.7"
$zipUrl = "https://gitlab.com/api/v4/projects/9642800/packages/generic/plandocument/$themeVersion/plandocument-$themeVersion.zip"

Write-Host "Deleting old version..."
Remove-Item $PSScriptRoot\$themeDir -Recurse -ErrorAction Ignore
$tmp = New-TemporaryFile | Rename-Item -NewName { $_ -replace 'tmp$', 'zip' } -PassThru
Write-Host "Downloading new version..."
Invoke-WebRequest -OutFile $tmp $zipUrl
Write-Host "Extracting archive version..."
$tmp | Expand-Archive -DestinationPath $PSScriptRoot\$themeDir -Force
Write-Host "Cleaning up..."
$tmp | Remove-Item
Write-Host "Theme updated to version: $themeVersion"
